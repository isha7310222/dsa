//WAP to check if given number is palindrome number or not
//using for loop
import java.io.*;
class Palindrome{
                public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter a number");
                int num=Integer.parseInt(br.readLine());

		int rev=0;
		int temp=num;
                while(num!=0){
                        int r=num%10;
			rev=rev*10+r;
                        num=num/10;
		}
                        if(rev==temp){

				System.out.println("Palindrome");
			}else{

                                System.out.println("Not Palindrome");
			}
        }
}

//using Recursion

class Palindrome2 {

    	static int palindrome(int num, int rev) {
        if (num == 0) {
            return rev;
        } else {
            int r = num % 10;
	    rev = rev * 10 + r;
            return palindrome(num / 10, rev);
        }
    }

    public static void main(String[] args) throws IOException {
        
	    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        
	    System.out.println("Enter a number");
        
	    int num = Integer.parseInt(br.readLine());

        
	    int reversedNum = palindrome(num, 0);
        
	    if (reversedNum == num) {
           
		    System.out.println("Palindrome");
        
	    } else {
            
		    System.out.println("Not Palindrome");
        
	    }
    }
}







