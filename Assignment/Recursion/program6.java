//6.WAP to calculate sum of digits of given positive integer
//using for loop
import java.io.*;
class Sum{
                public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter a number");
                int num=Integer.parseInt(br.readLine());

		int sum=0;
                while(num!=0){
		
		int r=num%10;
		sum=sum+r;
		num=num/10;
                }
		System.out.println(sum);
        }
}

//using Recursion
class Sum2{

        static int sum(int num){

		if(num==0){
			return 0;
		}

                return num%10+sum(num/10);

        }
        public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter a number");
                int num=Integer.parseInt(br.readLine());

                int ret=sum(num);
		System.out.println(ret);
              
        }

}
