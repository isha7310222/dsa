//2. WAP to display the first 10 natural numbers in reverse order.
//using for loop
import java.io.*;
class Reverse{
		public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter a number");
                int num=Integer.parseInt(br.readLine());

                for(int i=num;i>=1;i--){

                        System.out.println(i);
                }
		
        }
}

//using Recursion
class Reverse2{

        static void reverse(int num){

                if(num==0){
                        return ;
                }

                
                System.out.println(num);
		reverse(num-1);
		
        }
        public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter a number");
                int num=Integer.parseInt(br.readLine());

                reverse(num);
        }

}
