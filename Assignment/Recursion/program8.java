//WAP to count the occurance of specific digit in given number
//using for loop
import java.io.*;
class Occurance{
                public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter a number");
                int num=Integer.parseInt(br.readLine());

		System.out.println("Enter a digit to be found");
		int dig=Integer.parseInt(br.readLine());

		int count=0;
                while(num!=0){
			int r=num%10;
			num=num/10;
			if(r==dig){
				count++;
			}
                       
		}
                System.out.println("Count: "+count);
                

        }
}

//using Recursion
class Occurance2{

        static int count=0;

        static int occurance(long num,int dig){

		if(num%10==dig){
			
			count++;
		}
		if(num==0){
			return count;
		}
                return occurance(num/10,dig);

        }
        public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter a number");
                long num=Long.parseLong(br.readLine());

		
                System.out.println("Enter a digit to be found");
                int dig=Integer.parseInt(br.readLine());

                int ret=occurance(num,dig);

                System.out.println("Count: "+ret);
                
        }
}
