
//1. WAP to print the numbers between 1 to 10.
//using for loop
import java.io.*;
class Print{

	 public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter a number");
                int num=Integer.parseInt(br.readLine());

		for(int i=1;i<=num;i++){

			System.out.println(i);
		}
	}
}

//using Recursion
class Print2{

	static void print(int num){

		if(num==0){
			return ;
		}
		
		print(num-1);
		System.out.println(num);
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a number");
		int num=Integer.parseInt(br.readLine());

		print(num);
	}

}
		
