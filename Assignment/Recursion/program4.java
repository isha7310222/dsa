//4. WAP to print the length of digits in a number.
//using for loop
import java.io.*;
class Len{
                public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter a number");
                int num=Integer.parseInt(br.readLine());

                int count=0;
                while(num!=0){
		
			count++;
			num=num/10;
                }
                System.out.println("Len: "+count);

        }
}

//using Recursion
class Len2{

	static int count=0;
        static int len(int num){

                if(num==0){
			return count;
		}	
		count++;
		return  len(num/10); 


        }
        public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	
                System.out.println("Enter a number");
                int num=Integer.parseInt(br.readLine());

                int ret=len(num);
                System.out.println("Len : "+ret);
        }

}
