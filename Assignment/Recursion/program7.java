//WAP to find factorial of a number
//using for loop
import java.io.*;
class Factorial{
                public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter a number");
                int num=Integer.parseInt(br.readLine());

                int fact=1;
                for(int i=1;i<=num;i++){
                        fact=fact*i;
                
                }
               
                System.out.println(fact);
                

        }
}

//using Recursion
class Factorial2{

        static int fact(int num){

                if(num==1)
                        return 1;

        	return num*fact(num-1);
        }
        public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter a number");
                int num=Integer.parseInt(br.readLine());

                int ret=fact(num);
		System.out.println(ret);
        }

}
