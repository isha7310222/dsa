//WAP to print string in rev order
////using for loop
import java.io.*;
class Reverse{
                public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter a String");
                String str=br.readLine();

                char ch[]=str.toCharArray();
                int start=0;
		int end=ch.length-1;

		while(start<end){

			char temp=ch[start];
			ch[start]=ch[end];
			ch[end]=temp;
			start++;
			end--;
                }
		String result=new String(ch);
                System.out.println(result);


        }
}

//using Recursion
class Reverse2{

        static char[] reverse(char ch[],int start,int end){

             
		if(start>=end){
			return ch;
		}
		if(start<end){

			char temp=ch[start];
			ch[start]=ch[end];
			ch[end]=temp;

		}
                return reverse(ch,start+1,end-1);

        }
        public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter a String");
		String str=br.readLine();

		char ch[]=str.toCharArray();
		int start=0;
		int end=ch.length-1;

                char ch1[]=reverse(ch,start,end);

		String result=new String(ch1);
                System.out.println(result);

        }
}
