//3. WAP to print the sum of n natural numbers.
//using for loop
import java.io.*;
class Sum{
                public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter a number");
                int num=Integer.parseInt(br.readLine());

		int sum=0;
                for(int i=1;i<=num;i++){

                        sum=sum+i;
                }
		System.out.println(sum);

        }
}

//using Recursion
class Sum2{

        static int sum(int num){

                if(num==1){
                        return 1;
                }

               return num+sum(num-1);


        }
        public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter a number");
                int num=Integer.parseInt(br.readLine());

                int ret=sum(num);
		System.out.println("Sum : "+ret);
        }

}
