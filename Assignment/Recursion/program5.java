
//5. WAP to check whether the number is prime or not.
//using for loop
import java.io.*;
class Prime{
                public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter a number");
                int num=Integer.parseInt(br.readLine());

                int count=0;
                for(int i=1;i<=num;i++){
                        if(num%i==0)
			 count++;
                }
		if(count==2){
                System.out.println("Prime");
		}

        }
}

//using Recursion
class Prime2{
	
        static boolean prime(int num,int i){
                
		if(num<=2)
			return(num==2)?true:false;

		if(num%i==0)
			return false;

		if(i*i>num)
			return true;

		return prime(num,i+1);

        }
        public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter a number");
                int num=Integer.parseInt(br.readLine());

                boolean ret=prime(num,2);
		if(ret==true){
                System.out.println("Prime");
		}else{
			System.out.println("Not Prime");
		}
        }

}
