
//Given an element of array of size N find the sum of max and min in array

import java.io.*;

class MinMax{

	static int sumMinMax(int arr[]){

		int min=Integer.MAX_VALUE;
		int max=Integer.MIN_VALUE;

		for(int i=0;i<arr.length;i++){
			if(arr[i]>max){
				max=arr[i];
			}
			
                         if(arr[i]<min){

                                  min=arr[i];
                        }
		}
		return min+max;
	}

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter array Size");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());
		}
		int sum=sumMinMax(arr);

		System.out.println("sum: "+sum);

	}
}
