
//Range sum query
//Given an array A of length N

import java.io.*;

class RangeSum{

	static void rangesum(int arr[],int arr2[][],int row){

		int arr3[]=new int[row];

		for(int i=1;i<arr.length;i++){

			arr[i]=arr[i]+arr[i-1];
		}
		for(int i=0;i<arr2.length;i++){

			int start=0;
			int end=0;
			for(int j=0;j<2;j++){

				start=end;
				end=arr2[i][j];
			}
			arr3[i]=arr[end]-arr[start-1];
		}
		for(int x:arr3){

			System.out.print(x+" ");
		}
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter 1D array size");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];

		System.out.println("Enter 2D array row size");
		int row=Integer.parseInt(br.readLine());
		int arr2[][]=new int[row][2];	

		System.out.println("Enter 1D array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		System.out.println("Enter 2D array elements");
		for(int i=0;i<arr2.length;i++){

			for(int j=0;j<arr2[i].length;j++){

				do{
				arr2[i][j]=Integer.parseInt(br.readLine());

				if(arr2[i][j]>arr.length-1)
					System.out.println("Enter elements in Range");

			}while(arr2[i][j]>arr.length-1);
			
		}
		}
		rangesum(arr,arr2,row);
	}
}



		
