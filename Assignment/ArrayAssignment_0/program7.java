//Leaders in array
//Given an integer array A of size N distinct integers 
//You have to find all leaders in array.An element is leader is 
//it is strictly greater than all the elements from right side,

import java.io.*;
import java.util.*;
class Leaders{
	
	static void leaders(int arr[],int N){

		int rightMax[]=new int[N];
        	rightMax[N-1]=arr[N-1];
        
		for(int i=N-2;i>=0;i--){

                        if(arr[i]>rightMax[i+1]){

                                rightMax[i]=arr[i];
                }else{
                                rightMax[i]=rightMax[i+1];
                        }
		}
               System.out.println("Leaders");

	       HashSet<Integer> hs=new HashSet<>();

               for(int x:rightMax){

		       hs.add(x);
	       } 
	       System.out.println(hs);
	}        

	public static void main(String[] args)throws IOException{

                 BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter Size of Array");
                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];

                System.out.println("Enter array elements");
                for(int i=0;i<arr.length;i++){

                        arr[i]=Integer.parseInt(br.readLine());
                }
		leaders(arr,size);
	}
}
