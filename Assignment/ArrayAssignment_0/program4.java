
//Time to equality
//Given an array of size N.In one second you can increase the value of 1 element by 1.
//Find the min time in second to make each element equal.

import java.io.*;

class Equal{

	static int equal(int arr[]){

		int count=0;
	
		int max=Integer.MIN_VALUE;

		for(int i=0;i<arr.length;i++){

			if(arr[i]>max){
				max=arr[i];
	
			}
		}
		for(int i=0;i<arr.length;i++){

			for(int j=0;j<max;j++){

			if (arr[i]<max){

				count++;
				arr[i]=arr[i]+1;
			}
			else if(arr[i]==max){
				break;
			}

			}
		}
		return count;
	}
			
	 public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");
                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];
                System.out.println("Enter array elements");

                for(int i=0;i<arr.length;i++){

                        arr[i]=Integer.parseInt(br.readLine());
                }
	
		int count=equal(arr);
		System.out.println("Time: "+count+" sec");
	 }
}

