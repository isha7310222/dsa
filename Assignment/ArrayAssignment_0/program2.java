
//Linear search 
//Given an array of A and Integer B find the number of occurances of B in A

import java.io.*;

class Search{

	static int search(int arr[],int B){

		int count=0;
		for(int i=0;i<arr.length;i++){

			if(arr[i]==B){
				count++;
			}
		}
		return count;
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("Enter an integer to be found");

		int B=Integer.parseInt(br.readLine());

		int count=search(arr,B);

		System.out.println("Count: "+count);
	}
}

		
