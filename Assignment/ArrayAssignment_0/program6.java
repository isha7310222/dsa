
/*Given an array of integers A,
 * find and return the product array of the same size where the i th elemnent of the product array will be equal to
 * the product of all the elements divided by the i th element of the array.
 * It is always possible to form the product array with integer values.solve without using dividsion operator.
 */

import java.io.*;

class Product{

	     static void product(int arr[]){

		     int product[]=new int[arr.length];

		     for(int i=0;i<arr.length;i++){

			     int mult=1;
			     for(int j=0;j<arr.length;j++){

				     if(i!=j){
			    
					     mult=mult*arr[j];
				     }
			     }
			     product[i]=mult;
		     }
		     for(int x:product){

			     System.out.print(x+" ");
		     }
	     }
	     public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array Size");
                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];
                System.out.println("Enter array elements");

                for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());
		}

		product(arr);
	     }
}


