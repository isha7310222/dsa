/* Given an array of size N-1 such that it only contains distinct integers in the range of 1 to N.
 * find the missing element.
 * Ex-1
 * i/p=arr[]={1,2,3,5}
 * o/p=5
 */

import java.io.*;
class Missing{

	static void missing(int arr[],int N){

		boolean[] found=new boolean[N+1];

			for(int j=0;j<N-1;j++){
				
				if(arr[j]>=1 && arr[j]<=N){
				
					found[arr[j]]=true;
				}
			}
			for(int i=1;i<=N;i++){

			if(!found[i]){
				System.out.println("Missing: "+i);
			}
		}
	}
	public static void main(String[] args)throws IOException{

	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

	System.out.println("Enter array size");
	int N=Integer.parseInt(br.readLine());

	int arr[]=new int[N-1];

	System.out.println("Enter array elements");

	for(int i=0;i<arr.length;i++){

		arr[i]=Integer.parseInt(br.readLine());
	}

	missing(arr,N);
	}

}

