/*Given an arry of n positive numbers.The task is to find the first equilibrium point in ana array .
 * Equilibrium point in an array is an array index such that the sum of all elements before
 * that index is the same as the sum of elements after it.if nos uch element return -1.
 * ex-1
 * i/p=A[]={1,3,5,2,2}
 * o/p=3
 */

import java.io.*;
class Equilibrium{

        static int equiIndex(int arr[],int N){

                int front[]=new int[N];
                int back[]=new int [N];
                front[0]=arr[0];

                for(int i=1;i<arr.length;i++){

                        front[i]=front[i-1]+arr[i];
                }

                System.out.println("Front");
                for(int x:front){
                        System.out.println(x);

                }
                back[N-1]=arr[N-1];
                for(int i=N-2;i>0;i--){

                        back[i]=back[i+1]+arr[i];
                }
                  System.out.println("Back");
                for(int x:back){
                        System.out.println(x);
                }
                for(int i=0;i<arr.length;i++){

                        if(front[i]==back[i]){
                                return i;
                        }
                }
                return -1;
        }

        public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");
                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];

                System.out.println("Enter array elements");

                for(int i=0;i<arr.length;i++){

                        arr[i]=Integer.parseInt(br.readLine());
                }

                int ind=equiIndex(arr,size);

                System.out.println("Index :"+ind);

        }
}
