
/*Given a sorted array arr containing n elements with possibly some duplicate,the task
 *is to find the first and last occurances of an elements x in the given array.
 if not found return -1.
 */
import java.io.*;
class Index{

	static void index(int arr[],int x){

		int i=0;
		int temp=0;
		for(i=0;i<arr.length;i++){
			
			if(arr[i]==x){

				for(int j=i;j<arr.length;j++){

					if(arr[j]==x){
						temp=j;
						continue;
					}
					else{
						break;
					}
				}
				break;
			}
		}
		System.out.println(i+ " "+temp);

	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size");
		int size=Integer.parseInt(br.readLine());

		System.out.println("Enter array elements");
		int arr[]=new int[size];

		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());
		}
		System.out.println("Enter elemet to be found");
		int x=Integer.parseInt(br.readLine());

		index(arr,x);
	}
}


