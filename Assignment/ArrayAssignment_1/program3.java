
/*Given an string s and an array of string words, determine whether s is a prefix
 * String of words.A string s is a prefix string of words if s can be made by concatenating 
 * the first k strings in words for some positive k no larger than words.length.
 * Return true if s is a prefix string of words,or false otherwise.
 *Ex-1
 i/p:"iloveleetcode", words=["i","love","leetcode","apples"]
 o/p: True
 */

import java.io.*;

class SubArray{

	static boolean check(String s,String words[]){

		for(int i=1;i<words.length;i++){

			words[i]=words[i-1]+words[i];
		}
		for(int i=0;i<words.length;i++){

			if(s.equals(words[i])){
				return true;
			}
		}
		return false;
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a String");

		String s=br.readLine();

		System.out.println("Enter array size");
		int size=Integer.parseInt(br.readLine());

		System.out.println("Enter String array elements");

		String words[]=new String[size];

		for(int i=0;i<words.length;i++){

			words[i]=br.readLine();
		}

		boolean str=check(s,words);

		System.out.println(str);
	}
}
