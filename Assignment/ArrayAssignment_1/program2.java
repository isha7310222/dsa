/*Given an array of non-negative integers representing a number, implement a function to
simulate the carry forward operation that occurs when adding 1 to the number represented by
the array. The array represents the digits of the number, where the 0th index is the least
significant digit. Your task is to handle the carry forward operation correctly, updating the array
accordingly. The function should return the resulting array.
For example, given the input array [1, 9, 9], representing the number 199, the function should
return [2, 0, 0], representing the result of adding 1 to 199 with the carry forward properly
handled.
Consider edge cases such as when the number has trailing zeros or when the carry forward
results in an additional digit. Optimize your solution for efficiency and discuss the time and
space complexity of your algorithm.
*/
import java.io.*;

class CarryForward{

	static int[] addcarry(int arr[]){

		int carry=1;

		for(int i=arr.length-1;i>=0;i--){

			int sum=arr[i]+carry;
			 arr[i]=sum%10;
			 carry=sum/10;


	 		 if(carry==0){
	 
				 break;
			 }
		}
		if(carry>0){
			int result[]=new int[arr.length+1];
			result[0]=carry;
			
			for(int i=1;i<result.length;i++){

				result[i]=arr[i-1];
			}
			return result;
		}
		return arr;
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());
		}
		int result[]=addcarry(arr);

		System.out.println("New array...");

		for(int x:result){

			System.out.print(x+" ");
		}
	}
}

