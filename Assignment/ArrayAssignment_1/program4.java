
/* Given an array of positive integers nums and positive integer target,return the minimal length 
 * of a subarry whose sum is greater than or equal to target.If there is no such subarray,return 0.
 * Ex-1
 * i/p=target=7,nums=[2,3,1,2,4,3]
 * o/p=2
 */
import java.io.*;

class SubArraySum{
	
	static int length(int nums[],int target){

		int length=0;
		int sum=0;
		int minlen=Integer.MAX_VALUE;

		for(int i=0;i<nums.length;i++){

			sum=nums[i];
			for(int j=i+1;j<nums.length;j++){

			if(nums[i]==target){

				return 1;
			}
			sum=sum+nums[j];
			if(sum>=target){
				length=j-i+1;
			
			if(minlen>length)
				minlen=length;
			}
			}
		}
		if(minlen==Integer.MAX_VALUE){

			return 0;
		}
		return minlen;
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of array");
		int size=Integer.parseInt(br.readLine());

		int nums[]=new int[size];
		System.out.println("Enter array elements");

		for(int i=0;i<nums.length;i++){

			nums[i]=Integer.parseInt(br.readLine());
		}
		System.out.println("Enter target");
		int target=Integer.parseInt(br.readLine());

		int minLen=length(nums,target);
		System.out.println("Minlen: "+minLen);
	}
}

