
/*Given an array containing N integers. The problem id to find the sum of the 
 * elements of the contiguous subarray having a smallest min sum 
 * arr[]={3,-4,2,-3,-1}  o/p=-6
 * */
import java.io.*;
class SmallestSum{

	static int sum(int arr[]){
	
	int minSum=Integer.MAX_VALUE;
	int min=Integer.MAX_VALUE;
	for(int i=0;i<arr.length;i++){

		if(arr[i]<min){
			min=arr[i];
		}
	}
	for(int i=0;i<arr.length;i++){

		int temp=0;
		for(int j=i;j<arr.length;j++){
		
		        temp=temp+arr[j];
			if(temp<minSum){
				minSum=temp;
			}
		}
				
		}
	if(minSum==Integer.MAX_VALUE){

		return min;
	}
	
	return minSum;
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter array size");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());
		}
		int sum=sum(arr);

		System.out.println("Sum: "+sum);
	}
}
