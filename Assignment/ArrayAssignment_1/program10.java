/*You are given an array of soze N containing integers.Your task is to find 
 * the number of subarrays that can be formed from the given array. A subarray 
 * is defined as a contiguos sequence of elements in the array
 * arr=[1,2,3]
 */
import java.io.*;

class CountSubarray{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of array");
		int size=Integer.parseInt(br.readLine());

		System.out.println("Enter array elements");

		int arr[]=new int[size];
		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());
		}

		int subarrays=(size*(size+1))/2;

		System.out.println("No of subarrays: "+subarrays);
	}
}
