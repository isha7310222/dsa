/*Given an unsorted array arr[] of size N.Rotate the array to 
 * the left (clockwise) bu k steps whrew k is positi e integers.
 */
import java.io.*;
class Rotate{

        static void rotate(int arr[],int k){

                while(k!=0){

                        int store=arr[arr.length-1];
                        for(int i=arr.length-1;i>0;i--){

                                arr[i]=arr[i-1];
                        }
                        arr[0]=store;
                        k--;
                }

                System.out.println("Array after sorting");

                for(int x:arr){

                        System.out.println(x);

                }
        }
        public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");
                int size=Integer.parseInt(br.readLine());

                System.out.println("Enter array elements");
                int arr[]=new int[size];

                for(int i=0;i<arr.length;i++){

                        arr[i]=Integer.parseInt(br.readLine());
                }
                System.out.println("Enter no of rotation");
                int k=Integer.parseInt(br.readLine());

                rotate(arr,k);
        }
}

//TC=O(N*K)   SC=O(1]
