
/*Given an array Arr[] that contains N integers.Find the product of maximum product of subarray.
 * i/p=arr[]={6,-3,-10,0,2]
 * o/p=180
 */
import java.io.*;

class Product{

	static int maxproduct(int arr[]){

		for(int i=1;i<arr.length;i++){

			arr[i]=arr[i]*arr[i-1];
		}

		int max=Integer.MIN_VALUE;
		for(int i=0;i<arr.length;i++){
			if(arr[i]>max){
				max=arr[i];
			}
		}
		return max;
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		System.out.println("Enter array elements");
		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());
		}

		int max=maxproduct(arr);

		System.out.println("maxproduct: "+max);
	}

	
}

