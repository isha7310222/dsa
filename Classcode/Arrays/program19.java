
/* Given an characters array(lowerCase).Return the count of pairs (i,j)such that
 * a) i<j
 *  b) arr[i]='a'
 *      arr[j]='g'
 * Arr:[a,b,e,g,a,g]
 * o/p=3
 */
//Brute force

class Character{

	public static void main(String[] args){

		int N=6;
		char ch[]=new char[]{'a','b','e','g','a','g'};

		int count=0;
		for(int i=0;i<N;i++){

			for(int j=i+1;j<N;j++){

				if(ch[i]=='a' && ch[j]=='g')
					count++;

			}
		}
			System.out.println("count: "+count);
		
	}
}
