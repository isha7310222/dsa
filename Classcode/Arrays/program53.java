//print Maxsum of subarray of subarray of length 4 ;k=4
//Using prefix sum
import java.io.*;

class PrefixSubarray{

        static void prefixSubarray(int arr[],int K){

		int prefix[]=new int[arr.length];

		prefix[0]=arr[0];

		for(int i=1;i<arr.length;i++){

			prefix[i]=prefix[i-1]+arr[i];
		}

                int left=0;
                int right=K-1;
                int maxSum=Integer.MIN_VALUE;

                while(right<arr.length){

        		int sum=0;

			if(left==0){

				sum=prefix[right];
			}else{
				sum=prefix[right]-prefix[left-1];
			}
			if(maxSum<sum){
				maxSum=sum;
			}

                        left++;
                        right++;

                }
                System.out.println(maxSum);
        }
        public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");
                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];
                System.out.println("Enter array elements");

                for(int i=0;i<arr.length;i++){

                        arr[i]=Integer.parseInt(br.readLine());
                }
                System.out.println("Enter K");
                int K=Integer.parseInt(br.readLine());

                prefixSubarray(arr,K);
        }
}
