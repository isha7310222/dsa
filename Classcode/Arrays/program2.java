
/*Given an array of size N.count the no of elements having at least 1 element greater than itself.
  Arr:[2,5,1,4,8,0,8,1,3,8]
  N=10
  o/p=7 */
class Check{

	public static void main(String[] args){

		int N=10;
		int arr[]=new int[]{2,5,1,4,8,0,1,3,8};

		int itr=0;
		int count=0;
		for(int i=0;i<arr.length;i++){

			for(int j=i+1;j<arr.length;j++){

				itr++;
				if(arr[i]<arr[j]){
				count++;
				break;
				}
			}
		}
			System.out.println(count);
			System.out.println(itr);
		
	}

}
//TC=O(N^2)   SC=O(1)


