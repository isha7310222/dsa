
/* Given an array of size N.Return the count of pairs(i,j) with the Arr[i]+Arr[j]=k
 * Arr=[3,5,2,1,-3,7,8,15,6,13]
 * N=10
 * K=10
 * Constraint/Note= i!=j
 * o/p=6 */

class Pairs{

	public static void main(String[] args){

		int arr[]=new int[]{3,5,2,1,-3,7,8,15,6,13};

		int N=10;
		int K=10;
		int count=0;

		for(int i=0;i<arr.length;i++){

			for(int j=0;j<arr.length;j++){

				if(i!=j){
				
					if(arr[i]+arr[j]==K)
						count++;
				}
			}
		}
		System.out.println(count);
	}
}
//TC=O(N^2)   SC=O(1)
