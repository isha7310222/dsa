
/*Given an array of size N and Q number of queries. Query contains two psrsmeters(s,e)
 * s=> start idex and e=> end index for all queries; print the sum of all elements from
 * index s to index e.
 * arr==[-3,6,2,4,5,2,8,-9,3,1];
 * N=10
 * Q=3
 * Queries    s     e    sum
 * query 1:   1     3     12
 * query 2:   2     7     12 
 * query 3:   1     1     6
 */

import java.util.*;

class RangeSum{

	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);
		int N=10;
		int Q=3;
		
		int arr[]=new int[]{-3,6,2,4,5,2,8,-9,3,1};

		int sum;
		for(int i=0;i<Q;i++){

			sum=0;
			System.out.println("Enter start");
			int s=sc.nextInt();

			System.out.println("Enter end");
			int e=sc.nextInt();

			for(int j=s;j<=e;j++){

				sum=sum+arr[j];
			}
		System.out.println("Sum: "+sum);
		}
	}
}
//TC=O(Q*N)    SC=O(1)
