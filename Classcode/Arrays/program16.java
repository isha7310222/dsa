
/* Given an array of size N.Build an array leftmax of size N;
 * leftmax of i contans the maximum for the index 0 to index i
 * Arr:[-3,6,2,4,5,2,8,-9,3,1]
 * N=10
 * Leftmax:[-3,6,6,6,6,6,8,8,8,8]
 */

class LeftMax{

	public static void main(String[] args){

		int N=10;
		int arr[]=new int[]{-3,6,2,4,5,2,8,-9,3,1};
		int leftMax[]=new int[N];

		for(int i=0;i<arr.length;i++){

			int max=Integer.MIN_VALUE;

			for(int j=0;j<=i;j++){

				if(max<arr[j]){
					max=arr[j];
				}
			}
			leftMax[i]=max;
		}

		for(int x:leftMax){

			System.out.print(x+" ");
		}
	}
}
//TC=O(N^2)    SC=O(1)
