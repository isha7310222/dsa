
/*Given an array of size N
 * int arr[]=new int[]{2,4,1,3}
 * B]Print the sum of every single subarray
 *using prefix sum technique
 */
class PrefixSubarray{

	public static void main(String[] args){

		int arr[]=new int[]{2,4,1,3};

		int prefix[]=new int[arr.length];

		prefix[0]=arr[0];
		for(int i=1;i<arr.length;i++){

			prefix[i]=prefix[i-1]+arr[i];
		}

		for(int i=0;i<arr.length;i++){

			for(int j=i;j<arr.length;j++){

				int sum=0;

				if(i==0){
					sum=prefix[j];
				}
				else{
					sum=prefix[j]-prefix[i-1];
				}
				System.out.println(sum);
			}
		}
	}
}


