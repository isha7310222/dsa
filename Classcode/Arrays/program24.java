/* SubArray
 * Given an integer array of size N
 * Return the length of the smallest
 * subarray which contains both the 
 * max and min of array arr:[1,2,3,1,3,4,6,4,6,3]
 * */

class SubArray{

	public static void main(String[] args){

		int arr[]=new int[]{1,2,3,1,3,4,6,4,6,3};

		int minlength=Integer.MAX_VALUE;

		int min=Integer.MAX_VALUE;

		int max=Integer.MIN_VALUE;

		for(int i=0;i<arr.length;i++){

			if(arr[i]>max){
				max=arr[i];
			}
			if(arr[i]<min){

				min=arr[i];
			}
		}

		int len=0;
		for(int i=0;i<arr.length;i++){

			if(arr[i]==min){
				for(int j=i+1;j<arr.length;j++){

					if(arr[j]==max){

					len=j-i+1;
					if(minlength>len){
						minlength=len;
					}
					}
				}
			}else if(arr[i]==max){

				for(int j=i+1;j<arr.length;j++){

                                        if(arr[j]==min){

                                        len=j-i+1;
                                        if(minlength>len){
                                                minlength=len;
                                        }
                                        }
                                }
			}
		}
		System.out.println("Minlength: "+minlength);
	}
}

