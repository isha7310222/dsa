
//Find no of subarray of subarray of length 4 ;k=4
 
import java.io.*;

class CountSubarray{

	static void countSubarray(int arr[],int K){

		int left=0;
		int right=K-1;
		int count=0;

		while(right<arr.length){
			count++;
			left++;
			right++;
		}
		System.out.println("Count"+count);
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());
		}
		System.out.println("Enter K");
		int K=Integer.parseInt(br.readLine());

		countSubarray(arr,K);
	}
}


