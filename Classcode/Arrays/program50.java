
//Find no of subarray of subarray of length 4 ;k=4
//optimized
import java.io.*;

class CountSubarray1{

        static void countSubarray(int arr[],int K){

                int count=arr.length-K+1;
                System.out.println("Count"+count);
        }
        public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");
                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];
                System.out.println("Enter array elements");

                for(int i=0;i<arr.length;i++){

                        arr[i]=Integer.parseInt(br.readLine());
                }
                System.out.println("Enter K");
                int K=Integer.parseInt(br.readLine());

                countSubarray(arr,K);
        }
}
