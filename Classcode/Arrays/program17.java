

/* Given an array of size N.Build an array leftmax of size N;
 * leftmax of i contans the maximum for the index 0 to index i
 * Arr:[-3,6,2,4,5,2,8,-9,3,1]
 * N=10
 * Leftmax:[-3,6,6,6,6,6,8,8,8,8]
 */
//Optimized approach

class LeftMax{

        public static void main(String[] args){

                int N=10;
                int arr[]=new int[]{-3,6,2,4,5,2,8,-9,3,1};
                int leftMax[]=new int[N];

		leftMax[0]=arr[0];
                for(int i=1;i<arr.length;i++){

			if(arr[i]>leftMax[i-1]){

				leftMax[i]=arr[i];
			}else{
				leftMax[i]=leftMax[i-1];
			}
		}
		for(int x:leftMax){

			System.out.print(x+" ");
		}
	}
}
//TC=O(N)     SC=O(1)

