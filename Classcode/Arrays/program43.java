
/*Given a square matrix print right diagonal */

class Diagonal{

        public static void main(String[] args){

                int arr[][]={{1,2,3},{5,6,7},{9,10,11}};

                int i=0;
                int j=arr.length-1;
                while(i<arr.length && j>=0){

                        System.out.print(arr[i][j]+" ");
                        i++;
                        j--;
                }

        }
}

