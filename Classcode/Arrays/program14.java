

/* Given an array A of size N and integer B,you have to return the same array after rotating it B times towards the rights.
 * problem constraints
 * 1<=N<=10^5
 * 1<=A[i]<=10^9
 * 1<=B<=10^9
 * Input 1
 * A=[1,2,3,4]
 *
 * Input 2
 * A=[2,5,6]
 *
 * example output
 * Output 1
 * [3,4,1,2]
 *
 *
 * Output 2
 * [6,2,5]  */
import java.io.*;
class Rotate{
	
	static void rotate(int arr[],int k){

		while(k!=0){

			int store=arr[arr.length-1];
			for(int i=arr.length-1;i>0;i--){

				arr[i]=arr[i-1];
			}
			arr[0]=store;
			k--;
		}
	
		System.out.println("Array after sorting");
	
		for(int x:arr){
		
			System.out.println(x);
	
		}
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");
		int size=Integer.parseInt(br.readLine());

		System.out.println("Enter array elements");
		int arr[]=new int[size];

		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());
		}
		System.out.println("Enter no of rotation");
		int k=Integer.parseInt(br.readLine());

		rotate(arr,k);
	}
}

//TC=O(N*K)   SC=O(1]
