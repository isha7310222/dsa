

/* Given an array A of size N and integer B,you have to return the same array after rotating it B times towards the rights.
 * problem constraints
 * 1<=N<=10^5
 * 1<=A[i]<=10^9
 * 1<=B<=10^9
 * Input 1
 * A=[1,2,3,4]
 *
 * Input 2
 * A=[2,5,6]
 *
 * example output
 * Output 1
 * [3,4,1,2]
 *
 *
 * Output 2
 * [6,2,5]  */

import java.util.*;

class Rotation{

        static void rotation(int arr[],int b,Deque<Integer> deque){

                for(int i=0;i<b;i++){

                        int temp=deque.removeFirst();
                        deque.addLast(temp);
                }

                System.out.println("After rotation");

                for(int x:deque){

                        System.out.println(x);
                }
        }
        public static void main(String[] args){

                Scanner sc=new Scanner(System.in);

                System.out.println("Enter Size of Array");
                int size=sc.nextInt();

                System.out.println("Enter No of rotations");
                int b=sc.nextInt();

                int arr[]=new int[size];

                Deque<Integer> deque=new ArrayDeque<>();
                System.out.println("Enter array elements");
                for(int i=0;i<arr.length;i++){

                        arr[i]=sc.nextInt();
                        deque.add(arr[i]);
                }

                rotation(arr,b,deque);
        }
}
