/* Given an characters array(lowerCase).Return the count of pairs (i,j)such that
 * a) i<j
 *  b) arr[i]='a'
 *      arr[j]='g'
 * Arr:[a,b,e,g,a,g]
 * o/p=3
 */
//Optimized approach

class Character{

        public static void main(String[] args){

                int N=6;
                char ch[]=new char[]{'a','b','e','g','a','g'};

                int count=0;
                int aCount=0;

        
		for(int i=0;i<ch.length;i++) {
            
		    if (ch[i]== 'a') {
                
			    aCount++;
            
		    } else if (ch[i] == 'g') {
                
			    count=count+ aCount;
            
		    }
        
		}       
       	System.out.println("count: "+count);

        }
}
