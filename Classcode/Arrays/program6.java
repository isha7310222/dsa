
/* Given an array of size N. Reverse the array without using extra space..SC=O(1)
 * Arr=[8,4,1,3,9,2,6,7];
 * o/p=[7,6,2,9,3,1,4,8]
 */
//Optimized Approach
class ReverseArray{

	public static void main(String[] args){

		int N=8;

		int arr[]=new int[]{8,4,1,3,9,2,6,7};	

		int i=0;
		int j=N-1;
		while(i<j){
			int temp=arr[i];
			arr[i]=arr[j];
			arr[j]=temp;

			i++;
			j--;
		}
		for(int x:arr){

			System.out.println(x);
		}
	}
}
//TC=O(N)  SC=O(1)
