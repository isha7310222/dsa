
/*Given an array of size N.count the no of elements having at least 1 element greater than itself.
  Arr:[2,5,1,4,8,0,8,1,3,8]
  N=10
  o/p=7 */
//Optimized Appproach

class ArrayDemo{

	public static void main(String[] args){

		int N=10;

		int arr[]=new int[]{ 2,3,5,1,4,8,0,8,1,3,8};

		int max=Integer.MIN_VALUE;

		for(int i=0;i<arr.length;i++){

			if(max<arr[i]){

				max=arr[i];
			}
		}
		int count=0;
		for(int i=0;i<arr.length;i++){

			if(arr[i]==max){

				count++;
			}
		}
		System.out.println(N-count);
	}
}
//TC=O(N)   SC=O(1)

