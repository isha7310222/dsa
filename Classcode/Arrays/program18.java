
/* Given an array of size N.Build an array rightmax of size N;
 * rightmax of i contans the maximum for the index i to index N-1
 * Arr:[-3,6,2,4,5,2,8,-9,3,1]
 * N=10
 * Righttmax:[8,8,8,8,8,8,8,3,3,1]
 */
class RightMax{

        public static void main(String[] args){

                int N=10;
                int arr[]=new int[]{-3,6,2,4,5,2,8,-9,3,1};
                int rightMax[]=new int[N];

                rightMax[N-1]=arr[N-1];
                for(int i=N-2;i>=0;i--){

                        if(arr[i]>rightMax[i+1]){

                                rightMax[i]=arr[i];
                        }else{
                                rightMax[i]=rightMax[i+1];
                        }
                }
                for(int x:rightMax){

                        System.out.print(x+" ");
                }
        }
}
//TC=O(N)     SC=O(1)
