
/* Equilibrium index of an array
 * The equilibrium index of an array is an index such that the sum of elements
 *at lower indexes are equal to the sum of higher indexes
 if no eqilibrium return -1
 */

import java.io.*;
class Equilibrium{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());
		}
		int flag=0;

		for(int i=0;i<arr.length;i++){

			int leftsum=0;
			int rightsum=0;

			for(int j=0;j<i;j++){

				leftsum=leftsum+arr[j];
			}

			for(int j=i+1;j<arr.length;j++){

				rightsum=rightsum+arr[j];
			}
			if(rightsum==leftsum){

				flag=1;
				System.out.println("EquilibriumIndex:"+i);
				break;
			}
		}
		if(flag==0){

			System.out.println("-1");
		}
	}
}
