/*Print subarray with max sum
 * arr[]={-2,1,-3,4,-1,2,1,-5,4}
 */
class Subarray{

	public static void main(String[] args){

		int arr[]=new int[]{-2,1,-3,4,-1,2,1,-5,4};

		int maxSum=Integer.MIN_VALUE;
		int sum=0;
		int startInd=-1;
		int endInd=-1;
		int x=0;

		for(int i=0;i<arr.length;i++){

			if(sum==0){
				x=i;
			}
			sum=sum+arr[i];
			
			if(sum>maxSum){
				maxSum=sum;
				startInd=x;
				endInd=i;
			}
			if(sum<0){
				sum=0;
			}
		}
		for(int i=startInd;i<=endInd;i++){

			System.out.print(arr[i]+" ");
		}
		System.out.println();
	}
}
