
/*print Maxsum of subarray of subarray of length 4 ;k=4
 * Using sliding window
 */

import java.io.*;
class SlidingWindow{

        static void slidingWindow(int arr[],int K){

                int left=0;
                int right=K-1;
                int sum=0;
                for(int i=left;i<=right;i++){
		
			sum=sum+arr[i];
		}
		left=1;
		right=K;
		int maxSum=Integer.MIN_VALUE;


		while(right<arr.length){

			sum=sum-arr[left-1]+arr[right];

                                if(maxSum<sum){

                                        maxSum=sum;
                                }
                        left++;
                        right++;

                }
                System.out.println(maxSum);
        }
        public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");
                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];
                System.out.println("Enter array elements");

                for(int i=0;i<arr.length;i++){

                        arr[i]=Integer.parseInt(br.readLine());
                }
                System.out.println("Enter K");
                int K=Integer.parseInt(br.readLine());

                slidingWindow(arr,K);
        }
}
