//Prefix sum

import java.util.*;

class PrefixSum{

	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);
		int N=10;
		int Q=3;

		int arr[]=new int[]{-3,6,3,4,5,2,8,3,-1,4};

		int psArr[]=new int[N];

		psArr[0]=arr[0];

		//prefix sum
		for(int i=1;i<arr.length;i++){

			psArr[i]=psArr[i-1]+arr[i];
		}
		//queries
		int sum=0;
		for(int i=0;i<Q;i++){

			System.out.println("Enter start");
			int start=sc.nextInt();

			System.out.println("Enter end");
			int end=sc.nextInt();

			if(start==0){
				System.out.println("Sum: "+psArr[end]);
			}else{
				System.out.println("Sum: "+(psArr[end]-psArr[start-1]));
			}
		}
	}
}
