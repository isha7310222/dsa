
//Given an array of size N.Reverse the array using another array..SC=O(N)
 
class ArrayDemo{

	public static void main(String[] args){

		int N=10;

		int arr[]=new int[]{1,2,4,5,6,3,7,8,9,10};

		int arr1[]=new int[N];

		for(int i=N-1;i>=0;i--){

			arr1[i]=arr[i];
			System.out.println(arr1[i]);
		}
	}
}
//TC=O(N)  SC=O(N)
