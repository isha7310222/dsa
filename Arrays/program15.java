
//Find max value till i

import java.util.*;

class Max{

	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter i");

		int i=sc.nextInt();
		int N=10;

		int arr[]=new int[]{3,4,5,1,2,7,9,8};

		int max=Integer.MIN_VALUE;

		for(int j=0;j<=i;j++){

			if(max<arr[j]){
				max=arr[j];
			}
		}
		System.out.println("Max: "+max);
	}
}
//TC=O(N)    SC=O(1)
	
