
/* In place Prefix Sum
 * Given an array A of N integers
 * construct prefix sum of the array in the given array itself
 * return an array of integers denoting the prefix sum of the given array
 * problem constraints
 * 1<=N<=10^5
 * 1<=A[i]<=10^3
 * example Input
 * Input 1
 * A=[1,2,3,4,5]
 *
 * Input 2
 * A=[4,3,2]
 *
 * output 1=[1,3,6,10,15]
 * output 2=[4,7,9]  */
import java.util.*;
class Sum{

        static void range(int arr[],int q){

                Scanner sc=new Scanner(System.in);

                for(int i=1;i<arr.length;i++){

                        arr[i]=arr[i]+arr[i-1];
                }
                int sum=0;
                for(int i=1;i<=q;i++){

                        System.out.println("Enter start");
                        int start=sc.nextInt();

                        System.out.println("Enter end");
                        int end=sc.nextInt();

                        if(start!=0){
                                 sum=arr[end]-arr[start-1];
                        }else{
                                sum=arr[end];
                        }
                        System.out.println("Sum: "+sum);
                }
        }
        public static void main(String[] args){

                Scanner sc=new Scanner(System.in);

                System.out.println("Enter Size of Array");
                int size=sc.nextInt();

                int arr[]=new int[size];

                System.out.println("Enter array elements");
                for(int i=0;i<arr.length;i++){

                        arr[i]=sc.nextInt();
                }
                System.out.println("Enter no of queries");
                int q=sc.nextInt();

                range(arr,q);
        }
}

//TC=O(max(q,N)   SC=O(1)
