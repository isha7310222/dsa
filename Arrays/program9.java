
//Take start and end index from user and print sum of elements
import java.util.*;

class Sum{

	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		int arr[]=new int[]{2,3,3,11,7,9,4};

		System.out.println("Enter Start");
		int start=sc.nextInt();

		System.out.println("Enter end");
		int end=sc.nextInt();

		int sum=0;

		for(int i=start;i<=end;i++){

			sum=sum+arr[i];
		}
		System.out.println("Sum: "+sum);
	}
}
//TC=O(N)   SC=O(1)
