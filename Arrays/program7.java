
//Arr=[8,4,1,3,9,2,6,7]..find second largest element

class ArrayDemo{

	public static void main(String[] args){

		int N=8;
		int arr[]=new int[]{8,4,1,3,9,2,6,7};

		int max=Integer.MIN_VALUE;

		int temp=0;

		for(int i=0;i<N;i++){

			if(arr[i]>max){
				temp=max;
				max=arr[i];
			}
		}
		System.out.println(temp);
	}
}
//TC=O(N)   SC=O(1)
