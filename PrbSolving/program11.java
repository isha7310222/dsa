

//Count number between range including first..optimized...

import java.util.*;

class NumCount{

        static int countNum(int n1,int n2){

                int count=n2-n1+1;
                return count;
        }
        public static void main(String[] args){

                Scanner sc=new Scanner(System.in);

                int n1=sc.nextInt();
                int n2=sc.nextInt();

                int count=countNum(n1,n2);

                System.out.println("Count "+count);
        }
}
