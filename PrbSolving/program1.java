
//Factors..for constraint 10^9 it will fail

//No of iterations N
import java.io.*;

class Factors{

	static int findfactors(int num){

		int count=0;

		int itr=0;

		for(int i=1;i<=num;i++){

			itr++;
			if(num%i==0){
				count++;
			}
		}

		System.out.println(itr);
		return count;
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		int n=Integer.parseInt(br.readLine());

		int count=findfactors(n);

		System.out.println(count);
	}
}

