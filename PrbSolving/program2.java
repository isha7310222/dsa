//No of iterations N/2..optimizes way

import java.io.*;

class Factors{

        static int findfactors(double num){

                int count=0;

		int itr=0;
                for(int i=1;i<=num/2;i++){

			itr++;
                        if(num%i==0){
                                count++;
                        }
			
                }
		System.out.println(itr);
                return count+1;
        }
        public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                double n=Integer.parseInt(br.readLine());

                int count=findfactors(n);

                System.out.println(count);
        }
}
