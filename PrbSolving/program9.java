

//sum of N elements..optimized..T(1)

import java.io.*;

class Sum{

        static int sum(int N){

                int sum=0;
                sum=(N*(N+1))/2;
		return sum;
        }
        public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter number");

                int n=Integer.parseInt(br.readLine());

                int sum=sum(n);

                System.out.println("Sum "+sum);
        }
}
