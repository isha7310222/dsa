
//find SquareRoot for non-perfect square...N iterations..TC..o(N)

import java.io.*;

class SquareRoot{

        static int sqrt(int num){

                int itr=0;
		int val=0;
                for(int i=1;i<=num;i++){

                        itr++;
                        if(i*i<=num){
                                val=i;
                        }
                }
                System.out.println("itr "+itr);

                return val;

                }

        public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                int n=Integer.parseInt(br.readLine());

                System.out.println(sqrt(n));
        }
}
