
//sum of N elements..brute-force..TC(N)

import java.io.*;

class Sum{

	static int sum(int N){

		int sum=0;
		for(int i=0;i<=N;i++){

			sum=sum+i;
		}
		return sum;
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter number");

		int n=Integer.parseInt(br.readLine());

		int sum=sum(n);

		System.out.println("Sum "+sum);
	}
}
