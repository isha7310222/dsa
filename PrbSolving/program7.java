

//find SquareRoot ..Most optimized code..TC..log(sqrt(N))

import java.io.*;

class SquareRoot{

        static int sqrt(int num){

		int start=1;
		int end=num;
                int itr=0;
		int ans=0;
            
               while(start<=end){

		       int mid=(start+end)/2;
                        itr++;
                        if(mid*mid==num){
                                System.out.println("itr "+itr);
				return mid;
                        }
			else if(mid*mid>num){

				end=mid-1;
			}
			else{
				ans=mid;
				start=mid+1;
			}
                }
                System.out.println("itr "+itr);

                return ans;

                }

        public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                int n=Integer.parseInt(br.readLine());

                System.out.println(sqrt(n));
        }
}
