
//Count number between range including first..brute-force...

import java.util.*;

class NumCount{

	static int countNum(int n1,int n2){

		int count=0;
		for(int i=n1;i<=n2;i++){

			count++;
		}
		return count;
	}
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		int n1=sc.nextInt();
		int n2=sc.nextInt();

		int count=countNum(n1,n2);

		System.out.println("Count "+count);
	}
}

