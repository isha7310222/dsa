/* Two Sum (Leetcode:- 1)

Given an array of integer numbers and an integer target, return indices of the two numbers such that they add up to target

You may assume that each input would have exactly one solution, and you may not use the same element twice

You can return the answer in any order.

Example 1:

input: nums = [2.7.11,15], target = 9

Output: [0,1]

Explanation: Because nums[0] + nums[1]== 9, we return [0, 1].

Example 2:

Input: nums = [3,2,4], target = 6

Output: [1,2]

Example 3:

Input: nums = [3,3], target = 6

Output: [0,1]

Constraints:

2< nums.length <= 104 -109 < nums[i] <= 109 -109 <= target <= 109
i
Only one valid answer exists
*/
import java.io.*;
class Solution {
    
	public int[] twoSum(int[] nums,int target){
        int sum=0;
        int ret[]=new int[2];
        
	for(int i=0;i<nums.length;i++){
        
	    	for(int j=i+1;j<nums.length;j++){
            
			sum=nums[i]+nums[j];
                
			if(target==sum){
                    
				return new int[]{i,j};
            
			}
        
		}
        }
        return new int[]{};

    }
    public static void main(String [] args)throws IOException{
        
	    BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        
	    System.out.println("Enter array size");
	    int size=Integer.parseInt(br.readLine());
	    int nums[]=new int[size];

        
	    System.out.println("Enter aray elements");
        
	    for(int i=0;i<nums.length;i++){
            
		    nums[i]=Integer.parseInt(br.readLine());
        
	    }
        
	    System.out.println("Enter target");
            int target=Integer.parseInt(br.readLine());

        
	    Solution sol=new Solution();
        
	    int ret[]=sol.twoSum(nums,target);

        
	    for(int x:ret){
            
		    System.out.print(x+" ");
        
	    }
    }

}
