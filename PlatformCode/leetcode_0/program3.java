/*
Search Insert Position (LeetCode-35)

Given a sorted array of distinct integers and a target value, return the index if the target is found. If not, return the index where it would be if it were inserted in order.

You must write an algorithm with O(log n) runtime complexity.

Example 1:

Input: nums[1,3,5,6], target = 5

Output 2

Example 2:

Input: nums[1,3,5,6], target = 2

Output: 1

Example 3:

Input: nums[1,3,5,6], target = 7

Output: 4

Constraints:

1 < nums.length <= 104

-104< nums[i] <= 104

nums contains distinct values sorted in ascending order.

-104 < target < 104
*/
import java.io.*;
class Insert{

	static int insert(int nums[],int target){

		 int left = 0;
         
		 int right = nums.length - 1;
        
		 while (left <= right) {
            
			 int mid = left + (right - left) / 2;

            
			 if (nums[mid] == target) {
                
				 return mid;
            
			 } else if (nums[mid] < target) {
                
				 left = mid + 1;
            
			 } else {
				 right = mid - 1;
			 }
		 }
        return left;
    }
	public static void main(String[] args)throws IOException{

	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

	System.out.println("Enter array size");
	int size=Integer.parseInt(br.readLine());

	int nums[]=new int[size];
	System.out.println("Enter array elements");

	for(int i=0;i<nums.length;i++){

		nums[i]=Integer.parseInt(br.readLine());
	}
	System.out.println("Enter target");
	int target=Integer.parseInt(br.readLine());

	int index=insert(nums,target);

	System.out.println("Index: "+index);
	}
}


