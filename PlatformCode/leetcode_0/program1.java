/*1. Reverse Integer (Leetcode:- 7)

Given a signed 32-bit integer x, return x with its digits reversed. If reversing x causes the value to go outside the signed 32-bit integer range [-231, 231 11, then return 0.

Assume the environment does not allow you to store 64-bit integers (signed or unsigned).

Example 1:

Input: x = 123

Output: 321

Example 2:

Input: x = -123

Output -321

Example 3

Input x = 120

Output: 21

Constraints

-231 <= x <= 231-1
*/
import java.io.*;
class Solution {
 
     	public int reverse(int x){
     
		int rev=0;
	     	while(x!=0){
		int temp=x%10;
		x=x/10;
    
		if(rev>0 && rev>(Integer.MAX_VALUE-temp)/10 || rev<0 && rev<(Integer.MIN_VALUE -temp)/10){
         
			return 0;
		}
		rev=rev*10+temp;
	}
		

	return rev;
	}

    public static void main(String[] args)throws IOException{
    
	    BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        
	    System.out.println("Enter element to be reverse");
        
	    int x=Integer.parseInt(br.readLine());
        
	    Solution sol=new Solution();
        
	    int num=sol.reverse(x);
        
	    System.out.println(num);
    }

}
